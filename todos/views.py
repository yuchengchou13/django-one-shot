from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodolistForm, TodoItemForm

# POST
# create form object "variable = something"
# check if form is valid (filled out correctly and fully)
# if not valid, then re-render form with userinfo and error message
# if valid, data is "cleaned" by django into variable.cleaned_data['']
# check if password == password_confirm
# if not valid, double check and reenter until valid
# if valid, make a new user then login and redirect to the mainpage

# GET
# create instance of blank form
# put blank form into context dictionary

# render page with form


def todolist_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    context = {
        "todolist": todolist,
    }
    return render(request, "todos/todolist_detail.html", context)


def todolist_list(request):
    todolists = TodoList.objects.all()

    context = {"todolists": todolists}
    return render(request, "todos/todolist_list.html", context)


def todolist_create(request):
    if request.method == "POST":
        form = TodolistForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todolist_list")
    else:
        form = TodolistForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todolist_create.html", context)


def todolist_edit(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodolistForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todolist_list")
    else:
        form = TodolistForm(instance=todolist)

    context = {
        "form": form,
    }
    return render(request, "todos/todolist_edit.html", context)


def todolist_delete(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todolist_list")

    return render(request, "todos/todolist_delete.html")


def todoitem_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todolist_detail", id=todolist.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todoitem_create.html", context)


def todoitem_edit(request, id):
    todoitems = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitems)
        if form.is_valid():
            todoitems = form.save()
            return redirect("todolist_detail", id=todoitems.list.id)
    else:
        form = TodoItemForm(instance=todoitems)

    context = {
        "form": form,
    }
    return render(request, "todos/todoitem_edit.html", context)
