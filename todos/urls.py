from django.urls import path
from todos.views import (
    todolist_detail,
    todolist_list,
    todolist_create,
    todolist_edit,
    todolist_delete,
    todoitem_create,
    todoitem_edit,
)

# urlpatterns = [
# path("url path", function from views, name="name of function")
# ]

urlpatterns = [
    path("<int:id>/", todolist_detail, name="todolist_detail"),
    path("", todolist_list, name="todolist_list"),
    path("create/", todolist_create, name="todolist_create"),
    path("edit/<int:id>/", todolist_edit, name="todolist_edit"),
    path("delete/<int:id>/", todolist_delete, name="todolist_delete"),
    path("createitem/", todoitem_create, name="todoitem_create"),
    path("<int:id>/edititem/", todoitem_edit, name="todoitem_edit"),
]
