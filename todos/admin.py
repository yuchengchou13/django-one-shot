from django.contrib import admin

# import model from model.py
from todos.models import TodoList, TodoItem

# @admin.register(model name)
# class(model name)Admin(admin.ModelAdmin)
# list the display = ("")


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ("name", "id")


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date")
